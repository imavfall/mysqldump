CREATE DATABASE IF NOT EXISTS crush;
USE crush;

CREATE TABLE IF NOT EXISTS `User`
(
 `id`           int NOT NULL AUTO_INCREMENT ,
 `firstName`    varchar(45) NOT NULL ,
 `lastName`     varchar(45) NOT NULL ,
 `email`        varchar(45) NOT NULL UNIQUE,
 `created_date` date NOT NULL ,

PRIMARY KEY (`id`)
);


CREATE TABLE `Caracteristic`
(
 `user_id`    int NOT NULL ,
 `bio`        varchar(545) NOT NULL ,
 `birth_date` date NOT NULL ,
 `gender`     tinyint NOT NULL ,
 `age`        int NOT NULL,

PRIMARY KEY (`user_id`),
KEY `fkIdx_53` (`user_id`),
CONSTRAINT `FK_53` FOREIGN KEY `fkIdx_53` (`user_id`) REFERENCES `User` (`id`)
);

CREATE TABLE `Photo`
(
 `user_id` int NOT NULL ,
 `url`     varchar(145) NOT NULL ,
 `format`  varchar(5) NOT NULL ,
 `width`   int NOT NULL ,
 `height`  int NOT NULL ,

PRIMARY KEY (`user_id`),
KEY `fkIdx_90` (`user_id`),
CONSTRAINT `FK_90` FOREIGN KEY `fkIdx_90` (`user_id`) REFERENCES `Caracteristic` (`user_id`)
);



CREATE TABLE IF NOT EXISTS `Match`
(
 `match_id`       int NOT NULL AUTO_INCREMENT ,
 `first_user_id`  int NOT NULL ,
 `second_user_id` int NOT NULL ,
 `match_date`     date NOT NULL ,

PRIMARY KEY (`match_id`),
KEY `fkIdx_35` (`first_user_Id`),
CONSTRAINT `FK_35` FOREIGN KEY `fkIdx_35` (`first_user_Id`) REFERENCES `User` (`id`),
KEY `fkIdx_38` (`second_user_id`),
CONSTRAINT `FK_38` FOREIGN KEY `fkIdx_38` (`second_user_id`) REFERENCES `User` (`id`)
);



CREATE TABLE IF NOT EXISTS `Like`
(
 `user_id` int NOT NULL ,
 `id`      int NOT NULL AUTO_INCREMENT ,
 `status`  tinyint NOT NULL ,
 `sender`  int NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_44` (`user_id`),
CONSTRAINT `FK_44` FOREIGN KEY `fkIdx_44` (`user_id`) REFERENCES `User` (`id`),
KEY `fkIdx_48` (`sender`),
CONSTRAINT `FK_48` FOREIGN KEY `fkIdx_48` (`sender`) REFERENCES `User` (`id`)
);

CREATE TABLE IF NOT EXISTS `Message`
(
 `id`           int NOT NULL AUTO_INCREMENT ,
 `sent_date`    date NOT NULL ,
 `created_date` date NOT NULL ,
 `content`      varchar(1045) NOT NULL ,
 `from`         int NOT NULL ,
 `to`           int NOT NULL ,
 `match_id`     int NOT NULL ,

PRIMARY KEY (`id`),
KEY `fkIdx_22` (`from`),
CONSTRAINT `FK_22` FOREIGN KEY `fkIdx_22` (`from`) REFERENCES `User` (`id`),
KEY `fkIdx_25` (`to`),
CONSTRAINT `FK_25` FOREIGN KEY `fkIdx_25` (`to`) REFERENCES `User` (`id`),
KEY `fkIdx_77` (`match_id`),
CONSTRAINT `FK_77` FOREIGN KEY `fkIdx_77` (`match_id`) REFERENCES `Match` (`match_id`)
);
