FROM mysql:5.7.15

ENV MYSQL_DATABASE=crush \
    MYSQL_ROOT_PASSWORD=helloworld

ADD db.sql /docker-entrypoint-initdb.d

EXPOSE 3306
